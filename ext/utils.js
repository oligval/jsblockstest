/**
 * Created by Codder on 20.10.2015.
 */

document.addEventListener('DOMContentLoaded', inject());

function inject() {
	var utils = {};
	utils.ajax = function (url, callback, isAsync) {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (this.readyState === 4) {
				callback.call(this);
			}
		};

		xhr.open('GET', url, isAsync);
		xhr.send(null);
	};

	utils.template = function (name) {
		var tmplSrc = document.getElementsByTagName('x-template'),
			wrapper = document.createElement('div'),
			fragment = document.createDocumentFragment(),
			target = document.getElementsByTagName('article')[0],
			tmplPreRender = function (name) {
				var path = name;
				if (path.charAt(0) !== '.') {
					path = './templates/' + path + '.html';
				}
				utils.ajax(path, function () {
					wrapper.innerHTML = this.response;
				});
				[].slice.call(wrapper.children)
					.forEach(function (element) {
						if (element.tagName == 'X-TEMPLATE') {
							{
								tmplPreRender(element.getAttribute('name'));
							}
						}
						fragment.appendChild(element);
					});
				target.appendChild(fragment);
			};
		tmplPreRender(tmplSrc[0].getAttribute('name'));
	};
	window.utils = utils;
}